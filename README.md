# CKI example project

This repository contains the template for new CKI projects.

To use it, go to the [New project] page in the [cki-project] group, click
on `Create from template` → `Group` → `general` → `Use template`, and select
`Public` visibility.

## Hacking

To start, install [Python 3], [tox] and some auxiliary tools with

```shell
# Fedora
sudo dnf install python3 tox jq lolcat toilet
# Debian
sudo apt install python3 tox jq lolcat toilet
```

### With direnv

It is highly recommended to install [direnv] to handle setting up virtual Python
environments and environment variables per project:

```shell
# Fedora
sudo dnf install direnv
# Debian
sudo apt install direnv
```

Then, create a `.envrc` file via `direnv edit` with

```shell
shared_conf python

# environment variable settings for the project
export CKI_LOGGING_LEVEL=DEBUG
```

and install the project dependencies via

```shell
python3 -m pip install --force-reinstall -e .
```

### Without direnv

Install the project dependencies via

```shell
python3 -m pip install --user --force-reinstall -e .
```

### Troubles

If you run into any problems that seem to suggest an out-of-date or missing
dependency, rerun the corresponding `python3 -m pip` command above.

### Container images

This example project builds the `example` container image. The [public
documentation] contains more information on how the CKI project uses container
images.

## Submitting an MR

Please run `tox` before submitting any changes:

```shell
tox
```

At the end of a succesful review, your MR will normally only be _approved_.
If you are not a `Developer` or `Maintainer` of the project, please ask to get
the MR merged. Otherwise, you are expected to merge it yourself.

Whoever does the merging has to handle any 💥 fallout 💥 of the resulting
automated deployment of the new version! So don't do this on a Friday or right
before you finish your work day 😂.

[public documentation]: https://cki-project.org/docs/background/container-images/
[cki-project]: https://gitlab.com/cki-project/
[New project]: https://gitlab.com/projects/new?namespace_id=3970123
[Python 3]: https://www.python.org/
[tox]: https://tox.readthedocs.io/en/latest/
[direnv]: https://direnv.net/
